package main

import "fmt"

func FibonacciCache(n int) int {
	fibNums := make([]int, n+1, n+2)
	if n < 2 {
		fibNums = fibNums[0:2]
	}
	fibNums[0] = 0
	fibNums[1] = 1
	for i := 2; i <= n; i++ {
		fibNums[i] = fibNums[i-1] + fibNums[i-2]
	}
	return fibNums[n]
}

func main() {
	var number int
	fmt.Printf("Введите номер числа Фибоначчи: ")
	fmt.Scanf("%d\n", &number)

	fmt.Println(FibonacciCache(number))

}
