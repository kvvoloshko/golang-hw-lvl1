package main

import (
	"fmt"
)

func FibonacciRecursion(n int) int {
	if n <= 1 {
		return n
	}
	return FibonacciRecursion(n-1) + FibonacciRecursion(n-2)
}

func main() {
	var number int
	fmt.Printf("Введите номер числа Фибоначчи: ")
	fmt.Scanf("%d\n", &number)

	fmt.Println(FibonacciRecursion(number))

}
