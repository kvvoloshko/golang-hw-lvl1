package main

import (
	"fmt"
	"math"
	"math/big"
	"os"
)

func main() {

	for {
		var a, b float64
		fmt.Printf("Введите числa: ")
		fmt.Scanln(&a, &b)

		fmt.Printf("\nСложение: '+'\n" +
			"Вычитание: '-'\n" +
			"Умножение: '*'\n" +
			"Деление: '/'\n" +
			"Возведение числа a в степень b: '^'\n" +
			"Факториал числа a: '!'\n" +
			"Отмена операции и возврат к вводу чисел: exit\n" +
			"\nВведите операцию: ")
		var operation string
		fmt.Scanf("%s\n", &operation)

		switch operation {
		case "+":
			fmt.Printf("Результат: %.2f\n\n", a+b)
		case "-":
			fmt.Printf("Результат: %.2f\n\n", a-b)
		case "*":
			fmt.Printf("Результат: %.2f\n\n", a*b)
		case "/":
			if b == 0 {
				fmt.Println("На ноль делить нельзя!")
			} else {
				fmt.Printf("Результат: %.2f\n\n", a/b)
			}
		case "^":
			fmt.Printf("Результат: %.2f\n\n", math.Pow(a, b))
		case "!":
			if a > 0 && math.Mod(a, 1.0) == 0 {
				var c int64 = int64(a)
				x := new(big.Int)
				fmt.Printf("Результат: %d\n\n", x.MulRange(1, c))
			} else {
				fmt.Println("Число должно быть целым и положительным")
			}
		case "exit":
			os.Exit(1)
		default:
			fmt.Println("Такой операции нет")
		}

		fmt.Printf("\nСледующая операция: next\n" +
			"Выйти: exit\n\n")
		var choice string
		fmt.Scanf("%s", &choice)
		if choice == "exit" {
			os.Exit(1)
		} else {
			continue
		}
		//по непонятной причине при заходе на новую итерацию
		//на 15 строку компилятор не реагирует (польз. ввод), с отладчиком ничего не вышло -
		//погуглил как это решается с VS Code, покрутил json, но вместо отладки IDE просто
		//выполняет программу не реагируя на останов
		//если убрать код начиная с 57 строки то все работает как должно
	}
}
