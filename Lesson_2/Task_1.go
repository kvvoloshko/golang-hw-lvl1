package main

import "fmt"

func main() {

	var sideA, sideB float32

	fmt.Print("Введите первую сторону прямоугольника: ")
	fmt.Scanln(&sideA)

	fmt.Print("Введите вторую сторону прямоугольника: ")
	fmt.Scanln(&sideB)

	fmt.Printf("Площадь прямоугольника равна %f", sideA*sideB)

}
