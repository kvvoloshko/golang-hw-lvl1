package main

import (
	"fmt"
	"math"
)

func main() {
	var area, length, diameter float64

	fmt.Print("Введите площадь окружности: ")
	fmt.Scanln(&area)

	length = 2 * math.Pi * math.Sqrt(area/math.Pi)
	diameter = length / math.Pi

	fmt.Printf("\nДлина окружности равна %f", length)
	fmt.Printf("\nДиаметр окружности равен %f", diameter)
}
