package main

import "fmt"

func main() {

	var number int32

	fmt.Printf("Введите трёхзначное число: ")
	fmt.Scanln(&number)

	fmt.Printf("\nСотни: %d \nДесятки: %d \nЕдиницы: %d", number/100, number%100/10, number%10)

}
