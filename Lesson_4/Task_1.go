package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func InsertionSort(ar []int64) {
	for i := 1; i < len(ar); i++ {
		x := ar[i]
		j := i
		for ; j >= 1 && ar[j-1] > x; j-- {
			ar[j] = ar[j-1]
		}
		ar[j] = x
	}
}

func main() {
	inputNums := []int64{}
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Введите числа. Программа отсортирует их в порядке возрастания. Остановка ввода - ctrl + Z")
	for scanner.Scan() {
		num, err := strconv.ParseInt(scanner.Text(), 10, 64)
		if err != nil {
			panic(err)
		}
		inputNums = append(inputNums, num)
	}

	InsertionSort(inputNums)
	fmt.Println(inputNums)

}
